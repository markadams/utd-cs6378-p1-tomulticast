import socket
import sys
import json
from itertools import combinations
import logging

host = ''
port = 8787

node_count = int(sys.argv[1])

def run_server():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(node_count)
    
    logging.info('Listening on port %s' % port)

    connected = {}
    results = {}

    while len(connected) < node_count:
        conn, addr = s.accept()

        node_id = conn.recv(1024).strip()
        logging.info('Node (%s) has connected.' % node_id)

        connected[node_id] = conn

    node_ids = set(connected.keys())
    
    logging.info('Sending node list to all clients')

    for node, sock in connected.items():
        other_nodes = set(node_ids)
        other_nodes.remove(node)

        sock.send(json.dumps(list(other_nodes)) + '\n')
    
    logging.info('Waiting for results from clients')

    for node, sock in connected.items():

        result = ''

        while True:
            data = sock.recv(2048)
            if not data: break
            result = result + data

        result = json.loads(result)
        logging.info(result)
        results[node] = result[u'@items']
        logging.info('Received result from %s' % node)

    logging.info('Closing sockets')
    for sock in connected.values():
        sock.close()

    s.close()

    return results

def is_ordered(results, pair):
    nodeA = [(msg[u'sender'], msg[u'body']) for msg in results[pair[0]]]
    nodeB = [(msg[u'sender'], msg[u'body']) for msg in results[pair[1]]]
    
    logging.debug(nodeA)
    logging.debug(nodeB)

    nodeA = [msg for msg in nodeA if msg in nodeB]
    nodeB = [msg for msg in nodeB if msg in nodeA]
    
    return nodeA == nodeB

def test_is_ordered():
    results = {}

    results['node0'] = [
                {'sender': 'node1', 'body': 'Msg 1'},
                {'sender': 'node2', 'body': 'Msg 2'}
                ]

    results['node3'] = [
                {'sender': 'node1', 'body': 'Msg 1'},
                {'sender': 'node4', 'body': 'Msg 4'},
                {'sender': 'node2', 'body': 'Msg 2'}
                ]

    results['node4'] = [
            {'sender': 'node2', 'body': 'Msg 2'},
            {'sender': 'node1', 'body': 'Msg 1'}
        ]

    return results

def process_results(results):
    failed = False

    for pair in list(combinations(results.keys(), 2)):
        result = is_ordered(results, pair) 
        
        log_method = logging.info if result else logging.error
        log_method('Comparing %s and %s: %s' % (pair[0], pair[1], 'PASS' if result else 'FAIL'))

        if not result:
            failed = True
            logging.info('Node %s messages: %s' % (pair[0], results[pair[0]]))
            logging.info('Node %s messages: %s' % (pair[1], results[pair[1]]))
    
    return not failed


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(message)s')

    results = run_server()
    #results = test_is_ordered()

    final_result = process_results(results)

    logging.info('Final Result %s' % 'PASS' if final_result else 'FAIL')

    

               

