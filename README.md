# Totally Ordered Multicast Service
*Author: Mark Adams <mark@markadams.me>*  
*CS C378: Advanced Operating Systems, University of Texas at Dallas*

## Summary
The goal of this project is to create a totally ordered multicast service in Java.

* [Project Requirements](https://bitbucket.org/markadams/cs6378-p1-tomulticast/src/master/docs/project-description.pdf)
* [Skeen's Algorithm](https://bitbucket.org/markadams/cs6378-p1-tomulticast/src/master/docs/skeen-algorithm.pdf)

## Build Instructions
To build using ant, run "ant dist". A JAR file will be compiled and placed in dist/lib/

To execute a node, run "java -jar dist/lib/tomulticast.jar --config-url {url}".
To execute the control server, run "python control-server.py {num_nodes}

Sample configurations for running on localhost and netXX servers are in the config/ directory and available online
already at http://www.utdallas.edu/~mla104020/config/