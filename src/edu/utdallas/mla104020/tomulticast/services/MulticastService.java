package edu.utdallas.mla104020.tomulticast.services;

import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessageDelivered;

import java.io.IOException;

public abstract class MulticastService {
    public abstract String getNodeId();
    public abstract void m_send(ApplicationMessage msg);
    public abstract ApplicationMessageDelivered m_recv() throws InterruptedException;
    public abstract void start();
    public abstract void close() throws IOException;
}

