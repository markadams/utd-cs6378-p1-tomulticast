package edu.utdallas.mla104020.tomulticast.services;

import edu.utdallas.mla104020.tomulticast.channels.Channel;
import edu.utdallas.mla104020.tomulticast.channels.SctpNetworkChannel;
import edu.utdallas.mla104020.tomulticast.channels.TcpNetworkChannel;
import edu.utdallas.mla104020.tomulticast.clocks.DefaultLamportClock;
import edu.utdallas.mla104020.tomulticast.clocks.LamportClock;
import edu.utdallas.mla104020.tomulticast.messages.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by mark on 6/29/14.
 */
public class TotallyOrderedMulticastService extends MulticastService {
    private BlockingQueue<ApplicationMessage> pending_messages;
    private ConcurrentHashMap<String, String> undelivered_messages;
    private ConcurrentHashMap<String, InProcessMessageState> inprocess_messages;
    private PriorityBlockingQueue<ApplicationMessageDeliveredImpl> delivered_messages;

    private Channel messageChannel;
    private LamportClock clock;

    private Thread outgoingThread;
    private Thread incomingThread;


    public TotallyOrderedMulticastService() throws IOException {
        messageChannel = new TcpNetworkChannel(TcpNetworkChannel.DEFAULT_TCP_PORT);
        pending_messages = new LinkedBlockingQueue<>();
        inprocess_messages = new ConcurrentHashMap<>();
        undelivered_messages = new ConcurrentHashMap<>();
        delivered_messages = new PriorityBlockingQueue<>();
        clock = new DefaultLamportClock();
    }

    public TotallyOrderedMulticastService(int serverPort) throws IOException {
        messageChannel = new TcpNetworkChannel(serverPort);
        pending_messages = new LinkedBlockingQueue<>();
        inprocess_messages = new ConcurrentHashMap<>();
        undelivered_messages = new ConcurrentHashMap<>();
        delivered_messages = new PriorityBlockingQueue<>();
        clock = new DefaultLamportClock();
    }

    public TotallyOrderedMulticastService(Channel channel){
        messageChannel = channel;
        pending_messages = new LinkedBlockingQueue<>();
        inprocess_messages = new ConcurrentHashMap<>();
        undelivered_messages = new ConcurrentHashMap<>();
        delivered_messages = new PriorityBlockingQueue<>();
        clock = new DefaultLamportClock();
    }

    @Override
    public String getNodeId() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            return "localhost";
        }
    }

    @Override
    public void m_send(ApplicationMessage msg) {
        pending_messages.add(msg);
    }

    @Override
    public ApplicationMessageDelivered m_recv() throws InterruptedException {
        return delivered_messages.take();
    }

    @Override
    public void start() {
        outgoingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    while (true){
                        try {
                            processOutgoing();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (InterruptedException e) {

                }

            }
        });

        incomingThread = new Thread(new Runnable(){

            @Override
            public void run() {
                try{
                    while (true){
                        try {
                            processIncoming();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (InterruptedException e) {

                }

            }
        });

        incomingThread.start();
        outgoingThread.start();

    }

    @Override
    public void close() throws IOException {
        this.incomingThread.interrupt();
        this.outgoingThread.interrupt();
        this.messageChannel.close();
    }

    private void processOutgoing() throws InterruptedException, IOException {
        ApplicationMessage msg = this.pending_messages.take();
        int timestamp = clock.tick();
        String id = UUID.randomUUID().toString();

        InProcessMessageState inproc_msg = new InProcessMessageState(timestamp, msg.getRecipients());

        inprocess_messages.put(id, inproc_msg);

        for (String recip : msg.getRecipients()){
            MulticastMessage broadcastMsg = MulticastMessage.CreateBroadcastMessage(id, recip, getNodeId(), msg.getBody(), timestamp);
            messageChannel.send(broadcastMsg);
        }



    }

    private void processIncoming() throws IOException, InterruptedException {

        MulticastMessage msg = this.messageChannel.recv();

        switch(msg.getMessageType()){
            case BROADCAST:
                processBroadcast(msg);
                break;
            case PROPOSAL:
                processProposal(msg);
                break;
            case FINAL:
                processFinal(msg);
                break;
            default:
                return;
        }


    }

    private void processFinal(MulticastMessage msg) {
        String body = undelivered_messages.remove(msg.getId());
        int ts = msg.getTimestamp();

        clock.set(ts);

        delivered_messages.add(new ApplicationMessageDeliveredImpl(msg.getFromAddress(), body, ts));
    }

    private void processBroadcast(MulticastMessage msg) throws IOException {
        this.undelivered_messages.put(msg.getId(), msg.getBody());
        MulticastMessage mc_msg = MulticastMessage.CreateProposalMessage(msg, clock.tick());
        messageChannel.send(mc_msg);
    }

    private void processProposal(MulticastMessage msg) throws IOException {
        String id = msg.getId();

        InProcessMessageState state = inprocess_messages.get(id);
        state.setProposal(msg.getFromAddress(), msg.getTimestamp());

        if (!state.isFinal()){
            return;
        }

        int final_ts = state.getFinalTimestamp();

        for (String recip : state.getRecipients()){
            messageChannel.send(MulticastMessage.CreateFinalMessage(id, recip, getNodeId(), final_ts));
        }

        inprocess_messages.remove(id);
    }

    public int getPendingLength(){
        return pending_messages.size();
    }

    public int getUndeliveredLength(){
        return undelivered_messages.size();
    }

    public int getDeliveredLength(){
        return delivered_messages.size();
    }

    public int getInprocessLength(){
        return inprocess_messages.size();
    }

    public void setStateInProcess(ConcurrentHashMap<String, InProcessMessageState> state){
        this.inprocess_messages = state;
    }

    public void setStateUndelivered(ConcurrentHashMap<String, String> state){
        this.undelivered_messages = state;
    }

    public LamportClock getClock(){
        return this.clock;
    }



}
