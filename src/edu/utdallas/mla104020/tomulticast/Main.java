package edu.utdallas.mla104020.tomulticast;

import com.cedarsoftware.util.io.JsonReader;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessageDelivered;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessageDeliveredImpl;
import edu.utdallas.mla104020.tomulticast.services.TotallyOrderedMulticastService;
import org.apache.commons.cli.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static CommandLine ParseCommandLineArguments(String[] args){
        Options opts = new Options();
        Option opt = null;

        opt = OptionBuilder.withLongOpt("config-url")
                .withArgName("url")
                .hasArg(true)
                .isRequired(true)
                .withDescription("The URL for the node config file")
                .create();

        opts.addOption(opt);


        CommandLineParser parser = new BasicParser();
        try {
            return parser.parse(opts, args);
        } catch (MissingOptionException ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("tomulticast", opts, true);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        System.exit(1);
        return null;
    }

    public static Config GetConfig(String url) throws IOException {
        Config conf = null;

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response1 = httpclient.execute(httpGet);

        InputStreamReader reader = new InputStreamReader(response1.getEntity().getContent());
        BufferedReader bufreader = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();
        String line = null;

        while ((line = bufreader.readLine()) != null){
            sb.append(line);
        }

        conf = (Config) JsonReader.jsonToJava(sb.toString());

        return conf;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        CommandLine opts = ParseCommandLineArguments(args);

        Config conf = GetConfig(opts.getOptionValue("config-url"));
        String[] parts = conf.control.split(":");
        System.out.println("Configuration downloaded... initiating control server protocol");

        ControlServerProtocol control = new ControlServerProtocol(parts[0], Integer.parseInt(parts[1]));
        System.out.println("Registered node... awaiting start signal...");
        control.RegisterNode(conf.id + ":" + conf.port);

        TotallyOrderedMulticastService service = new TotallyOrderedMulticastService(conf.port);
        service.start();
        control.GetNodeList();

        Thread.sleep(500);

        System.out.println("Beginning message processing...");

        for (int i = 0; i < 10; i++){
            if (conf.messages.containsKey(String.valueOf(i + 1))){
                ApplicationMessage msg = conf.messages.get(String.valueOf(i+1));
                service.m_send(msg);
                System.out.println("T=" + (i+1) + ": Sent message: " + msg.getBody());
            }

            Thread.sleep(1000);
        }

        Thread.sleep(2000);

        LinkedList<ApplicationMessageDelivered> messages = new LinkedList<>();

        while (service.getDeliveredLength() != 0){
            ApplicationMessageDelivered msg = service.m_recv();
            System.out.println("Received message from " + msg.getSender() + ": " + msg.getBody());
            messages.add(msg);
        }

        control.PublishResults(messages);
        System.exit(0);
    }
}
