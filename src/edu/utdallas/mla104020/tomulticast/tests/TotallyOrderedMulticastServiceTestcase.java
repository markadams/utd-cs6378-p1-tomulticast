package edu.utdallas.mla104020.tomulticast.tests;

import edu.utdallas.mla104020.tomulticast.channels.LocalChannel;
import edu.utdallas.mla104020.tomulticast.messages.*;
import edu.utdallas.mla104020.tomulticast.services.TotallyOrderedMulticastService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;

/**
 * Created by mark on 6/29/14.
 */
public class TotallyOrderedMulticastServiceTestcase {

    protected TotallyOrderedMulticastService service;

    @Before
    public void setup() throws IOException {
        service = new TotallyOrderedMulticastService(new LocalChannel());
    }

    @After
    public void teardown() throws IOException {
        service.close();
    }

    @Test
    public void testSendShouldAddMessageToPendingQueue(){
        ApplicationMessage msg = new ApplicationMessage(new HashSet<>(Arrays.asList("1")), "Hello World!");

        assertEquals(service.getPendingLength(), 0);

        service.m_send(msg);

        assertEquals(service.getPendingLength(), 1);
    }

    @Test(timeout = 5000)
    public void testPendingQueueMessagesShouldBeSentOnChannel() throws InterruptedException, IOException {
        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");

        ApplicationMessage msg = new ApplicationMessage(recipients, "Hello World!");
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        service.start();

        service.m_send(msg);

        MulticastMessage sentMulticast = channel.OutgoingMessages.take();

        assertEquals(msg.getBody(), sentMulticast.getBody());
        assertEquals(msg.getRecipients().toArray()[0], sentMulticast.getToAddress());
    }

    @Test(timeout = 5000)
    public void testPendingQueueMessagesShouldBePlacedInProcessQueue() throws InterruptedException, IOException {
        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");

        ApplicationMessage msg = new ApplicationMessage(recipients, "Hello World!");
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);


        service.m_send(msg);
        assertEquals(1, service.getPendingLength());

        service.start();
        Thread.sleep(100);

        assertEquals(0,service.getPendingLength());
        assertEquals(1, service.getInprocessLength());
    }

    @Test(timeout = 5000)
    public void testLamportClockShouldBeUsedOnSentMessages() throws InterruptedException, IOException {
        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");

        ApplicationMessage msg1 = new ApplicationMessage(recipients, "Hello World!");
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        service.start();

        service.m_send(msg1);

        ApplicationMessage msg2 = new ApplicationMessage(recipients, "Hello again!");
        service.m_send(msg2);

        MulticastMessage sentMulticast = channel.OutgoingMessages.take();

        assertEquals(msg1.getBody(), sentMulticast.getBody());
        assertEquals(sentMulticast.getTimestamp(), 1);

        sentMulticast = channel.OutgoingMessages.take();

        assertEquals(msg2.getBody(), sentMulticast.getBody());
        assertEquals(sentMulticast.getTimestamp(), 2);
    }

    @Test(timeout = 5000)
    public void testSameIdShouldBeUsedForMessageWithMultipleRecip() throws InterruptedException, IOException {
        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");
        recipients.add("127.0.0.2");

        ApplicationMessage msg1 = new ApplicationMessage(recipients, "Hello World!");
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        service.start();

        service.m_send(msg1);


        MulticastMessage sentMulticast = channel.OutgoingMessages.take();

        String id1 = sentMulticast.getId();

        sentMulticast = channel.OutgoingMessages.take();

        String id2 = sentMulticast.getId();

        assertEquals(id1, id2);
    }


    @Test(timeout = 5000)
    public void testWhenProposalReceivedAddTsToState() throws InterruptedException, IOException {

        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        String id = UUID.randomUUID().toString();
        int ts = 15;

        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.2");
        recipients.add("127.0.0.3");

        ConcurrentHashMap<String, InProcessMessageState> inprocess_state = new ConcurrentHashMap<>();
        InProcessMessageState msg_state = new InProcessMessageState(ts, recipients);
        inprocess_state.put(id, msg_state);

        service.setStateInProcess(inprocess_state);

        MulticastMessage incomingMessage = MulticastMessage.CreateProposalMessage(
                MulticastMessage.CreateBroadcastMessage(id, recipients.get(0), "127.0.0.1", "hello", ts),
                20
        );

        channel.IncomingMessages.put(incomingMessage);

        assertEquals(0, inprocess_state.get(id).getProposalCount());
        service.start();

        Thread.sleep(200);

        assertEquals(1, inprocess_state.get(id).getProposalCount());
    }

    @Test(timeout = 5000)
    public void testWhenAllProposalReceivedSendFinal() throws InterruptedException, IOException {

        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        String id = UUID.randomUUID().toString();
        int ts = 15;


        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.2");

        ConcurrentHashMap<String, InProcessMessageState> inprocess_state = new ConcurrentHashMap<>();
        InProcessMessageState msg_state = new InProcessMessageState(ts, recipients);
        inprocess_state.put(id, msg_state);

        service.setStateInProcess(inprocess_state);

        int proposal_ts = 20;

        MulticastMessage incomingMessage = MulticastMessage.CreateProposalMessage(
                MulticastMessage.CreateBroadcastMessage(id, recipients.get(0), "127.0.0.1", "hello", ts),
                proposal_ts
        );

        channel.IncomingMessages.put(incomingMessage);
        service.start();

        Thread.sleep(200);

        MulticastMessage finalMultiMessage = channel.OutgoingMessages.take();

        assertEquals(id, finalMultiMessage.getId());
        assertEquals(MessageType.FINAL, finalMultiMessage.getMessageType());
        assertEquals(proposal_ts, finalMultiMessage.getTimestamp());
        assertEquals(0, service.getInprocessLength());
    }

    @Test(timeout = 5000)
    public void testWhenBroadcastReceivedAddToUndelivered() throws IOException, InterruptedException {
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");

        String id = UUID.randomUUID().toString();
        int ts = 15;

        MulticastMessage incomingMessage = MulticastMessage.CreateBroadcastMessage(id, recipients.get(0), "127.0.0.1", "hello", ts);
        channel.IncomingMessages.put(incomingMessage);

        service.start();
        Thread.sleep(200);

        assertEquals(1, service.getUndeliveredLength());
    }

    @Test(timeout = 5000)
    public void testWhenBroadcastReceivedSendProposal() throws IOException, InterruptedException {
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");

        String id = UUID.randomUUID().toString();
        int ts = 15;

        MulticastMessage incomingMessage = MulticastMessage.CreateBroadcastMessage(id, recipients.get(0), "127.0.0.1", "hello", ts);
        channel.IncomingMessages.put(incomingMessage);

        service.start();
        Thread.sleep(200);

        MulticastMessage outgoing = MulticastMessage.fromJSON(channel.OutgoingMessages.take().toJSON());
        assertEquals(id, outgoing.getId());
        assertEquals(MessageType.PROPOSAL, outgoing.getMessageType());

    }

    @Test(timeout = 5000)
    public void testWhenFinalReceivedDeliverMessage() throws IOException, InterruptedException {
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        String messageBody = "Hello World!";
        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");

        String id = UUID.randomUUID().toString();
        int ts = 15;

        ConcurrentHashMap<String, String> undelivered = new ConcurrentHashMap<>();
        undelivered.put(id, messageBody);
        service.setStateUndelivered(undelivered);

        MulticastMessage incomingMessage = MulticastMessage.CreateFinalMessage(id, recipients.get(0), "192.168.1.1", ts);
        channel.IncomingMessages.put(incomingMessage);

        assertEquals(0, service.getDeliveredLength());
        assertEquals(1, service.getUndeliveredLength());

        service.start();
        Thread.sleep(200);

        assertEquals(0, service.getUndeliveredLength());
        assertEquals(1, service.getDeliveredLength());
    }

    @Test(timeout=5000)
    public void testWhenDeliveredMessageIsReadyRecvShouldRetrieve() throws IOException, InterruptedException {
        LocalChannel channel = new LocalChannel();
        service = new TotallyOrderedMulticastService(channel);

        String sender = "192.168.1.1";
        String messageBody = "Hello World!";
        LinkedList<String> recipients = new LinkedList<>();
        recipients.add("127.0.0.1");

        String id = UUID.randomUUID().toString();
        int ts = 15;

        ConcurrentHashMap<String, String> undelivered = new ConcurrentHashMap<>();
        undelivered.put(id, messageBody);
        service.setStateUndelivered(undelivered);

        MulticastMessage incomingMessage = MulticastMessage.CreateFinalMessage(id, recipients.get(0), sender, ts);
        channel.IncomingMessages.put(incomingMessage);

        service.start();

        Thread.sleep(200);

        ApplicationMessageDelivered msg = service.m_recv();

        assertEquals(messageBody, msg.getBody());
        assertEquals(sender, msg.getSender());

    }








}
