package edu.utdallas.mla104020.tomulticast.tests;

import edu.utdallas.mla104020.tomulticast.channels.Channel;
import edu.utdallas.mla104020.tomulticast.channels.TcpNetworkChannel;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessageDelivered;
import edu.utdallas.mla104020.tomulticast.services.MulticastService;
import edu.utdallas.mla104020.tomulticast.services.TotallyOrderedMulticastService;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mark on 7/8/14.
 */
public class IntegrationTestCase {
    @Test
    public void test_communication_between_nodes() throws IOException, InterruptedException {
        int node1_port = TcpNetworkChannel.DEFAULT_TCP_PORT;
        int node2_port = TcpNetworkChannel.DEFAULT_TCP_PORT + 1;

        MulticastService node1 = new TotallyOrderedMulticastService(node1_port);
        MulticastService node2 = new TotallyOrderedMulticastService(node2_port);

        node1.start();
        node2.start();

        List<String> node1_recip = new ArrayList<>();
        node1_recip.add("localhost:" + node2_port);

        ApplicationMessage app_msg_1 = new ApplicationMessage(node1_recip, "Hi Node 2!");

        List<String> node2_recip = new ArrayList<>();
        node2_recip.add("localhost:" + node1_port);

        ApplicationMessage app_msg_2 = new ApplicationMessage(node2_recip, "Hi Node 1!");

        node1.m_send(app_msg_1);
        node2.m_send(app_msg_2);

        ApplicationMessageDelivered recv_1 = node1.m_recv();
        ApplicationMessageDelivered recv_2 = node2.m_recv();

        assertEquals(app_msg_2.getBody(), recv_1.getBody());
        assertEquals(app_msg_1.getBody(), recv_2.getBody());
    }
}
