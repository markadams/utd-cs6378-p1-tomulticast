package edu.utdallas.mla104020.tomulticast.tests;

import edu.utdallas.mla104020.tomulticast.messages.MulticastMessage;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by mark on 7/2/14.
 */
public class MulticastMessageTestCase {
    @Test
    public void JsonDeserializedShouldMatchOriginal() throws IOException {
        MulticastMessage message = MulticastMessage.CreateBroadcastMessage("msg_id", "127.0.0.1", "127.0.0.2", "Hello World!", 10);

        String jsonMessage = message.toJSON();

        MulticastMessage unpacked = MulticastMessage.fromJSON(jsonMessage);

        assertEquals(message.getId(), unpacked.getId());
        assertEquals(message.getMessageType(), unpacked.getMessageType());
        assertEquals(message.getFromAddress(), unpacked.getFromAddress());
        assertEquals(message.getToAddress(), unpacked.getToAddress());
        assertEquals(message.getBody(), unpacked.getBody());
        assertEquals(message.getTimestamp(), unpacked.getTimestamp());

    }
}
