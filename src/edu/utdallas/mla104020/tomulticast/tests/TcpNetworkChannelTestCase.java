package edu.utdallas.mla104020.tomulticast.tests;
import edu.utdallas.mla104020.tomulticast.messages.ChannelMessage;
import edu.utdallas.mla104020.tomulticast.messages.ChannelMessageType;
import edu.utdallas.mla104020.tomulticast.messages.MulticastMessage;
import edu.utdallas.mla104020.tomulticast.channels.Channel;
import edu.utdallas.mla104020.tomulticast.channels.TcpNetworkChannel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mark on 6/22/14.
 */
public class TcpNetworkChannelTestCase {
    public Channel channel;
    public final int port = TcpNetworkChannel.DEFAULT_TCP_PORT + 1000;
    private final long TIMEOUT = 100;

    @Before
    public void begin() throws IOException {
        this.channel = new TcpNetworkChannel(port);
    }

    @After
    public void after() throws IOException {
        if (this.channel.isOpen()){
            this.channel.close();
        }
    }

    @Test(timeout=1000)
    public void recv_should_include_sending_ip() throws IOException, InterruptedException {
        String message = "Hello World";

        Thread t = new Thread(this.channel);
        t.start();

        SendTestMessage(message, this.port);

        Thread.sleep(TIMEOUT);

        MulticastMessage msg = this.channel.recv();
        assertEquals(msg.getFromAddress(), String.format("127.0.0.1:%d", this.port));


    }

    @Test(timeout=1000)
    public void recv_should_get_incoming_message() throws InterruptedException {
        try{

            String message = "Hello World!";


            Thread t = new Thread(this.channel);
            t.start();

            SendTestMessage(message, port);
            MulticastMessage msg = this.channel.recv();

            assertEquals(msg.getBody(), message);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test(timeout=1000)
    public void recv_should_get_two_incoming_messages_separately() throws InterruptedException {
        try{
            String message1 = "Hello World!";
            String message2 = "Goodbye World!";

            Thread t = new Thread(this.channel);
            t.start();

            SendTestMessage(message1, port);
            SendTestMessage(message2, port);

            MulticastMessage msg;

            msg = this.channel.recv();
            assertEquals(msg.getBody(), message1);
            msg = this.channel.recv();
            assertEquals(msg.getBody(), message2);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected=IOException.class, timeout = 5000)
    public void recv_should_raise_io_exception_if_closed_and_empty() throws IOException, InterruptedException {
        this.channel.close();
        this.channel.recv();
    }

    @Test(timeout=1000)
    public void recv_should_get_remaining_messages_if_closed_and_not_empty() throws IOException, InterruptedException {
        String expected = "Hello World!!!!";

        Thread t = new Thread(this.channel);
        t.start();

        SendTestMessage(expected, this.port);
        Thread.sleep(TIMEOUT);
        this.channel.close();

        MulticastMessage msg = this.channel.recv();
        assertEquals(expected, msg.getBody());

    }

    @Test(timeout=5000)
    public void send_should_send_message_to_destination() throws IOException, InterruptedException {
        String expected = "Hello world!!!!!!";

        Thread t = new Thread(this.channel);
        t.start();

        Thread.sleep(TIMEOUT);

        TcpNetworkChannel sendingChannel = new TcpNetworkChannel(this.port + 1);
        sendingChannel.send(MulticastMessage.CreateBroadcastMessage("1","127.0.0.1:" + port, "127.0.0.1:12345", expected, 15));

        Thread.sleep(TIMEOUT);

        MulticastMessage msg = this.channel.recv();
        sendingChannel.close();

        assertEquals(expected, msg.getBody());

    }

    @Test(timeout=5000)
    public void send_should_send_message_to_multiple_destinations() throws IOException, InterruptedException {
        String expected = "Hello world!!!!!";

        String destination = String.format("127.0.0.1:%d", 12345);
        String source = String.format("127.0.0.1:%d", 12346);

        List<MulticastMessage> messages = new ArrayList<>();
        messages.add(MulticastMessage.CreateBroadcastMessage("1","127.0.0.1:" + port, "127.0.0.1:12345", expected, 15));
        messages.add(MulticastMessage.CreateBroadcastMessage("2","127.0.0.1:" + port, "127.0.0.1:12345", expected, 15));

        Thread t = new Thread(this.channel);
        t.start();

        TcpNetworkChannel sendingChannel = new TcpNetworkChannel(this.port + 1);
        sendingChannel.send(messages);

        Thread.sleep(TIMEOUT);

        MulticastMessage msg1 = this.channel.recv();
        MulticastMessage msg2 = this.channel.recv();
        sendingChannel.close();

        assertEquals(expected, msg1.getBody());
        assertEquals(expected, msg2.getBody());
    }

    private void SendTestMessage(String message, int port) throws IOException {
        MulticastMessage msg = MulticastMessage.CreateBroadcastMessage("1","127.0.0.1:12345", String.format("127.0.0.1:%d", port), message, 15);
        Socket sock = new Socket("127.0.0.1", port);
        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
        output.write(msg.toJSON());
        output.close();
        sock.close();
    }

}
