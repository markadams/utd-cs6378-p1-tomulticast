package edu.utdallas.mla104020.tomulticast.tests;
import edu.utdallas.mla104020.tomulticast.clocks.DefaultLamportClock;
import edu.utdallas.mla104020.tomulticast.clocks.LamportClock;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by mark on 6/21/14.
 */
public class LamportTestCase {
    private LamportClock clock;
    private final int initial = 0;

    @Before
    public void setup(){
        reset_clock_to_zero();
    }

    public void reset_clock_to_zero(){
        this.clock = new DefaultLamportClock(0);
    }

    @Test
    public void test_default_constructor_time_is_0(){
        int expected = 0;
        this.clock = new DefaultLamportClock();

        int actual = this.clock.peek();
        assertEquals(expected, actual);
    }

    @Test
    public void test_constructor_initial_time_sets_time(){
        int expected = 20;
        this.clock = new DefaultLamportClock(expected);

        int actual = this.clock.peek();
        assertEquals(expected, actual);
    }

    @Test
    public void test_peek_should_return_current_time(){
        int expected = 0;
        int actual = this.clock.peek();

        assertEquals(expected, actual);
    }

    @Test
    public void test_peek_should_not_increment_time(){
        reset_clock_to_zero();

        int expected = initial;
        int actual_first = this.clock.peek();
        int actual_second = this.clock.peek();

        assertEquals(expected, actual_first);
        assertEquals(expected, actual_second);
    }

    @Test
    public void test_tick_should_return_value(){
        int expected = initial + 1;
        int actual = this.clock.tick();

        assertEquals(expected, actual);
    }

    @Test
    public void test_tick_should_increment_clock(){
        int expected = initial + 1;

        this.clock.tick();

        int actual = this.clock.peek();

        assertEquals(expected, actual);
    }

    @Test
    public void test_set_should_set_clock_to_value_plus_1_if_greater(){
        int expected = 11;

        this.clock.set(10);

        int actual = this.clock.peek();
        assertEquals(expected, actual);

    }

    @Test
    public void test_set_should_set_clock_to_time_plus_1_if_lesser(){
        int expected = 16;

        this.clock = new DefaultLamportClock(15);

        this.clock.set(13);

        int actual = this.clock.peek();
        assertEquals(expected, actual);

    }
}
