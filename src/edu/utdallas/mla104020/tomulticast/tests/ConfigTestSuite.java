package edu.utdallas.mla104020.tomulticast.tests;

import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;
import edu.utdallas.mla104020.tomulticast.Config;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by mark on 7/8/14.
 */
public class ConfigTestSuite {
    //@Test
    public void test_config_deserializes() throws IOException {
        String json = "{\"@type\":\"edu.utdallas.mla104020.tomulticast.Config\",\"control\":\"localhost:8787\",\"id\":\"127.0.0.1\",\"port\":5555,\"messages\":{\"@keys\":[\"1\"],\"@items\":[{\"@type\":\"edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage\",\"recipients\":[\"localhost:2345\",\"localhost:1234\"],\"body\":\"Hello World\"}]}}";
        ApplicationMessage msg = (ApplicationMessage)JsonReader.jsonToJava(json);
    }

    @Test
    public void test_config_serialize() throws IOException {
        Config conf = new Config();
        conf.control = "localhost:8787";
        conf.id = "127.0.0.1";
        conf.port = 5555;
        ApplicationMessage msg = new ApplicationMessage(new LinkedList<String>(), "Hello World");
        msg.recipients.add("localhost:1234");
        msg.recipients.add("localhost:2345");
        conf.messages.put("1", msg);
        String json = JsonWriter.objectToJson(conf);

        assertTrue(true);
    }
}
