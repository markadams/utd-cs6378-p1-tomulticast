package edu.utdallas.mla104020.tomulticast.tests;

import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage;
import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import static org.junit.Assert.*;


/**
 * Created by mark on 6/29/14.
 */
public class ApplicationMessageTestCase {

    @Test
    public void testGetBodyShouldReturnMessageBody(){
        String expected = "Hello everyone!";

        ApplicationMessage msg = new ApplicationMessage(new LinkedList<String>(), expected);
        String actual = msg.getBody();

        assertEquals(expected, actual);
    }

    @Test
    public void testGetRecipientsShouldReturnRecipients(){
        Set<String> expected = new HashSet<String>();
        expected.add("A");
        expected.add("B");
        expected.add("C");

        ApplicationMessage msg = new ApplicationMessage(expected, "");

        Set<String> actual = new HashSet<>(msg.getRecipients());
        assertTrue(actual.containsAll(expected));

        actual.removeAll(expected);
        assertTrue(actual.isEmpty());
    }
}
