package edu.utdallas.mla104020.tomulticast;

import com.cedarsoftware.util.io.JsonWriter;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage;
import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessageDelivered;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;

/**
 * Created by mark on 7/8/14.
 */
public class ControlServerProtocol {
    private Socket socket;

    public ControlServerProtocol(String host, int port) throws IOException {
        this.socket = new Socket(host, port);
    }

    public void RegisterNode(String node_id) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream());
        BufferedWriter sw = new BufferedWriter(writer);
        sw.write(node_id);
        sw.flush();
    }

    public void GetNodeList() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String nodelist = reader.readLine();
    }

    public void PublishResults(LinkedList<ApplicationMessageDelivered> messages) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream());
        BufferedWriter sw = new BufferedWriter(writer);

        ApplicationMessageDelivered[] delivered = new ApplicationMessageDelivered[messages.size()];
        messages.toArray(delivered);

        sw.write(JsonWriter.objectToJson(delivered));
        sw.close();
    }

    public void Close() throws IOException {
        this.socket.close();
    }
}
