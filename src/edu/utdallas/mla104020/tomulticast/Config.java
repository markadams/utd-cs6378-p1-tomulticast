package edu.utdallas.mla104020.tomulticast;

import edu.utdallas.mla104020.tomulticast.messages.ApplicationMessage;

import java.util.Dictionary;
import java.util.HashMap;

/**
 * Created by mark on 7/8/14.
 */
public class Config {
    public Config(){
        messages = new HashMap<String, ApplicationMessage>();
    }
    public String control;
    public String id;
    public int port;
    public HashMap<String, ApplicationMessage> messages;

}
