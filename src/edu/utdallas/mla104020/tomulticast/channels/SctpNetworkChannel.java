package edu.utdallas.mla104020.tomulticast.channels;

import com.sun.nio.sctp.MessageInfo;
import com.sun.nio.sctp.SctpChannel;
import com.sun.nio.sctp.SctpServerChannel;
import edu.utdallas.mla104020.tomulticast.messages.MulticastMessage;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * EXPERIMENTAL: Not for Production Use
 */
public class SctpNetworkChannel extends Channel {
    private final int port;
    private SctpServerChannel socket;
    private boolean listening;
    private LinkedBlockingQueue<MulticastMessage> incomingQueue = new LinkedBlockingQueue<MulticastMessage>();
    private Thread receiver;

    public static final int DEFAULT_TCP_PORT = 5454;

    public SctpNetworkChannel(int port) throws IOException {
        this.port = port;
        this.socket = SctpServerChannel.open();
        this.socket.bind(new InetSocketAddress(port));
        this.receiver = new Thread(this);
        this.receiver.start();
    }


    public void send(MulticastMessage msg) throws IOException {
        String[] addrParts = msg.getToAddress().split(":");

        InetAddress address;
        int port;

        address = InetAddress.getByName(addrParts[0]);
        port = Integer.parseInt(addrParts[1]);

        if (!msg.getFromAddress().endsWith(String.valueOf(this.port))){
            msg.setFromAddress(msg.getFromAddress() + ":" + this.port);
        }

        SctpChannel client = SctpChannel.open(new InetSocketAddress(address, port),1,1);
        ByteBuffer buf = ByteBuffer.allocateDirect(2048);
        CharBuffer charbuf = CharBuffer.allocate(2048);
        Charset charset = Charset.forName("ASCII");
        CharsetEncoder encoder = charset.newEncoder();

        charbuf.put(msg.toJSON());
        encoder.encode(charbuf, buf, true);
        buf.flip();

        MessageInfo info = MessageInfo.createOutgoing(null,1);
        client.send(buf,info);
        client.close();
    }

    @Override
    public MulticastMessage recv() throws IOException {
        if ((!this.socket.isOpen()) && incomingQueue.size() == 0){
            throw new IOException("Channel has already been closed and no more messages remain to be received.");
        }

        try{
            return incomingQueue.take();
        } catch (InterruptedException e) {
            return null;
        }
    }

    @Override
    public void close() throws IOException {
        if (this.socket == null){
            return;
        }

        if (this.socket.isOpen()){
            this.socket.close();
        }

        if (this.receiver.isAlive()){
            this.receiver.interrupt();
        }
    }

    @Override
    public boolean isOpen() {
        return this.socket.isOpen();
    }

    @Override
    public void run() {
        try {
            ByteBuffer buf = ByteBuffer.allocateDirect(2048);
            Charset charset = Charset.forName("ASCII");
            CharsetDecoder decoder = charset.newDecoder();

            MessageInfo msginfo = null;

            while (socket.isOpen()) {
                SctpChannel sock = socket.accept();
                msginfo = sock.receive(buf, null, null);
                buf.flip();

                assert msginfo.isComplete();

                String body = decoder.decode(buf).toString();

                MulticastMessage msg = MulticastMessage.fromJSON(body);

                incomingQueue.add(msg);
                buf.clear();
                sock.close();
            }
        } catch (SocketException e){
            
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                if (this.isOpen()){
                    this.close();
                }

            } catch (IOException e) {

            }
        }
    }
}
