package edu.utdallas.mla104020.tomulticast.channels;

import com.cedarsoftware.util.io.JsonWriter;
import edu.utdallas.mla104020.tomulticast.messages.ChannelMessage;
import edu.utdallas.mla104020.tomulticast.messages.ChannelMessageType;
import edu.utdallas.mla104020.tomulticast.messages.MulticastMessage;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.*;
import java.net.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by mark on 6/22/14.
 */
public class TcpNetworkChannel extends Channel {
    private final int port;
    private ServerSocket socket;
    private boolean listening;
    private LinkedBlockingQueue<MulticastMessage> incomingQueue = new LinkedBlockingQueue<MulticastMessage>();
    private Thread receiver;

    public static final int DEFAULT_TCP_PORT = 5454;

    public TcpNetworkChannel(int port) throws IOException {
        this.port = port;
        this.socket = new ServerSocket(port);
        this.receiver = new Thread(this);
        this.receiver.start();
    }


    public void send(MulticastMessage msg) throws IOException {
        String[] addrParts = msg.getToAddress().split(":");

        InetAddress address;
        int port;

        address = InetAddress.getByName(addrParts[0]);
        port = Integer.parseInt(addrParts[1]);

        if (!msg.getFromAddress().endsWith(String.valueOf(this.port))){
            msg.setFromAddress(msg.getFromAddress() + ":" + this.port);
        }

        Socket client = new Socket(address, port);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));

        writer.write(msg.toJSON());
        writer.close();
        client.close();
    }

    @Override
    public MulticastMessage recv() throws IOException {
        if ((this.socket.isClosed()) && incomingQueue.size() == 0){
            throw new IOException("Channel has already been closed and no more messages remain to be received.");
        }

        try{
            return incomingQueue.take();
        } catch (InterruptedException e) {
            return null;
        }
    }

    @Override
    public void close() throws IOException {
        if (this.socket == null){
            return;
        }

        if (!this.socket.isClosed()){
            this.socket.close();
        }

        if (this.receiver.isAlive()){
            this.receiver.interrupt();
        }
    }

    @Override
    public boolean isOpen() {
        return !this.socket.isClosed();
    }

    @Override
    public void run() {
        try {
            while (!socket.isClosed()) {
                Socket sock = socket.accept();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(sock.getInputStream()));

                String body = in.readLine();

                MulticastMessage msg = MulticastMessage.fromJSON(body);

                incomingQueue.add(msg);
                sock.close();
            }
        } catch (SocketException e){
            
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                if (this.isOpen()){
                    this.close();
                }

            } catch (IOException e) {

            }
        }
    }
}
