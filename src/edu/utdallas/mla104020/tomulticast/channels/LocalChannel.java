package edu.utdallas.mla104020.tomulticast.channels;

import edu.utdallas.mla104020.tomulticast.messages.ChannelMessage;
import edu.utdallas.mla104020.tomulticast.messages.MulticastMessage;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by mark on 6/29/14.
 */
public class LocalChannel extends Channel {
    public LinkedBlockingQueue<MulticastMessage> OutgoingMessages;
    public LinkedBlockingQueue<MulticastMessage> IncomingMessages;

    public LocalChannel(){
        OutgoingMessages = new LinkedBlockingQueue<>();
        IncomingMessages = new LinkedBlockingQueue<>();
    }

    @Override
    public void send(MulticastMessage msg) throws IOException {
        try {
            OutgoingMessages.put(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public MulticastMessage recv() throws IOException, InterruptedException {

        return IncomingMessages.take();
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public void run() {

    }
}
