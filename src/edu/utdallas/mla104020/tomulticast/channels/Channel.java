package edu.utdallas.mla104020.tomulticast.channels;

import edu.utdallas.mla104020.tomulticast.messages.ChannelMessage;
import edu.utdallas.mla104020.tomulticast.messages.MulticastMessage;

import java.io.IOException;
import java.util.Collection;

/**
 * Created by mark on 6/22/14.
 */
public abstract class Channel implements Runnable{
    public abstract void send(MulticastMessage msg) throws IOException;
    public abstract MulticastMessage recv() throws IOException, InterruptedException;
    public abstract void close() throws IOException;
    public abstract boolean isOpen();

    public void send(Collection<MulticastMessage> messages) throws IOException{
        for (MulticastMessage msg : messages){
            this.send(msg);
        }
    }

}
