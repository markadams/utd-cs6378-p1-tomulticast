package edu.utdallas.mla104020.tomulticast.clocks;

/**
 * Created by mark on 6/21/14.
 */
public class DefaultLamportClock extends LamportClock {
    protected int time;

    public DefaultLamportClock(){
        this.time = 0;
    }

    public DefaultLamportClock(int initial_time){
        this.time = initial_time;
    }

    @Override
    public synchronized int tick() {
        this.time = this.time + 1;

        return this.time;
    }

    @Override
    public synchronized void set(int value) {
        this.time = Math.max(value, this.time) + 1;
    }

    @Override
    public synchronized int peek() {
        return time;
    }
}
