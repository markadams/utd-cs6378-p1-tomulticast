package edu.utdallas.mla104020.tomulticast.clocks;

/**
 * Created by mark on 6/21/14.
 */
public abstract class LamportClock {
    /***
     * Increments the clock
     * @return The new value of the clock
     */
    public abstract int tick();

    /***
     * Sets the counter to be greater than the maximum of its own clock value and the specified value.
     */
    public abstract void set(int value);

    /***
     * Gets the next value of the clock without incrementing the clock
     * @return The next value of the clock
     */
    public abstract int peek();

}
