package edu.utdallas.mla104020.tomulticast.messages;

import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;

import java.io.IOException;
import java.util.Map;

public class MulticastMessage {
    private String fromAddress;
    private String toAddress;
    private String body;
    private String id;
    private int timestamp;
    private MessageType type;

    public static MulticastMessage CreateBroadcastMessage(String id, String toAddress, String fromAddress, String body, int timestamp){
        return new MulticastMessage(id, MessageType.BROADCAST, toAddress, fromAddress, body, timestamp);
    }

    public static MulticastMessage CreateProposalMessage(MulticastMessage broadcastMessage, int timestamp){
        return new MulticastMessage(broadcastMessage.id, MessageType.PROPOSAL, broadcastMessage.fromAddress, broadcastMessage.toAddress, null, timestamp);
    }

    public static MulticastMessage CreateFinalMessage(String id, String toAddress, String fromAddress, int timestamp){
        return new MulticastMessage(id, MessageType.FINAL, toAddress, fromAddress, null, timestamp);
    }

    private MulticastMessage(String id, MessageType type, String toAddress, String fromAddress, String body, int timestamp){
        this.id = id;
        this.type = type;
        this.toAddress = toAddress;
        this.fromAddress = fromAddress;
        this.body = body;
        this.timestamp = timestamp;

    }

    public String getFromAddress(){
        return fromAddress;
    }

    public String getToAddress(){
        return toAddress;
    }

    public String getId(){
        return id;
    }

    public int getTimestamp(){
        return timestamp;
    }

    public MessageType getMessageType(){
        return type;
    }

    public String getBody(){
        return body;
    }

    public String toJSON() throws IOException {
        return JsonWriter.objectToJson(this);
    }

    public void setFromAddress(String value){
        fromAddress = value;
    }

    public static MulticastMessage fromJSON(String json) throws IOException {
        return (MulticastMessage)JsonReader.jsonToJava(json);
    }

}
