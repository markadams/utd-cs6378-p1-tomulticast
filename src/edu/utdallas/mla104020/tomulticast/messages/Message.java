package edu.utdallas.mla104020.tomulticast.messages;

/**
 * Created by mark on 6/22/14.
 */
public class Message {
    public int to_id;
    public int from_id;
    public String msg_id;
    public String body;
}
