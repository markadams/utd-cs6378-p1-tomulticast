package edu.utdallas.mla104020.tomulticast.messages;

/**
 * Created by mark on 7/2/14.
 */
public enum ChannelMessageType {
    SEND,
    RECV
}
