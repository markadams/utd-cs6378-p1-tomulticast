package edu.utdallas.mla104020.tomulticast.messages;

import java.util.Collection;

/**
 * Created by mark on 7/3/14.
 */
public interface ApplicationMessageDelivered {
    String getSender();
    String getBody();
}

