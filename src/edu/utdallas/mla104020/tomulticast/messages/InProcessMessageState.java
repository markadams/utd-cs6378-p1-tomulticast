package edu.utdallas.mla104020.tomulticast.messages;

import java.util.*;

/**
 * Created by mark on 7/2/14.
 */
public class InProcessMessageState {
    private int timestamp;
    private Map<String,Integer> recipients;

    public InProcessMessageState(int timestamp, Collection<String> recipients){
        this.timestamp = timestamp;
        this.recipients = new HashMap<String,Integer>();

        for (String recip : recipients){
            this.recipients.put(recip, null);
        }
    }

    public Collection<String> getRecipients(){
        return this.recipients.keySet();
    }

    public void setProposal(String recip, int ts){
        this.recipients.put(recip, ts);
    }

    public boolean isFinal(){
        for (String recip : recipients.keySet()){
            if (recipients.get(recip) == null){
                return false;
            }
        }

        return true;
    }

    public int getFinalTimestamp(){
        Set<Integer> timestamps = new HashSet(recipients.values());
        timestamps.add(timestamp);

        int max = -1;

        for (Integer ts : timestamps){
            if (ts > max){
                max = ts;
            }
        }

        return max;
    }

    public int getProposalCount(){
        int count = 0;

        for (Integer ts : recipients.values()){
            if (ts != null){
                count++;
            }
        }

        return count;
    }


}

