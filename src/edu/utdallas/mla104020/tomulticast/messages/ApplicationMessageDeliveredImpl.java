package edu.utdallas.mla104020.tomulticast.messages;

public class ApplicationMessageDeliveredImpl implements ApplicationMessageDelivered, Comparable<ApplicationMessageDeliveredImpl> {

    private String sender;
    private String body;
    private Integer ts;

    public ApplicationMessageDeliveredImpl(String sender, String body, int ts){
        this.sender = sender;
        this.body = body;
        this.ts = ts;
    }

    public String getSender(){
        return this.sender;
    }

    public String getBody(){
        return this.body;
    }

    public Integer getTimestamp(){
        return this.ts;
    }

    @Override
    public int compareTo(ApplicationMessageDeliveredImpl receivedApplicationMessage) {
        return this.getTimestamp().compareTo(receivedApplicationMessage.getTimestamp());
    }
}
