package edu.utdallas.mla104020.tomulticast.messages;

import java.util.*;

/**
 * Created by mark on 6/29/14.
 */
public class ApplicationMessage {
    public HashSet<String> recipients;
    public String body;

    public ApplicationMessage(Collection<String> recipients, String body){
        this.recipients = new HashSet<>(recipients);
        this.body = body;
    }

    public Collection<String> getRecipients() {
        return new LinkedList<>(recipients);
    }

    public String getBody(){
        return body;
    }
}
