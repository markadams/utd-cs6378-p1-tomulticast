package edu.utdallas.mla104020.tomulticast.messages;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by mark on 7/2/14.
 */
public class ChannelMessage {
    private ChannelMessageType messageType;
    private String remoteNodeAddress;
    private String body;


    public ChannelMessage(ChannelMessageType type, String remoteNodeAddress, String body) throws UnknownHostException {
        this.messageType = type;
        this.remoteNodeAddress = remoteNodeAddress;
        this.body = body;
    }

    public static ChannelMessage CreateOutgoing(MulticastMessage msg) throws IOException {
        String body = msg.toJSON();

        return new ChannelMessage(ChannelMessageType.SEND, msg.getToAddress(), body);

    }

    public ChannelMessageType getMessageType(){
        return messageType;
    }
    public String getRemoteNodeAddress(){
        return remoteNodeAddress;
    }
    public String getBody(){
        return body;
    }
}
