package edu.utdallas.mla104020.tomulticast.messages;

/**
 * Created by mark on 6/29/14.
 */
public enum MessageType {
    BROADCAST,
    PROPOSAL,
    FINAL
}
